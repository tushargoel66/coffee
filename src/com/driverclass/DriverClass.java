package com.driverclass;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.impl.Cs5Implementation;

public class DriverClass {
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
		Cs5Implementation cs5Implementation=new Cs5Implementation();
		while(true) {
			System.out.println("Enter your choice: ");
			System.out.println(" 1. Add ");
			System.out.println(" 2. Display Orders ");
			System.out.println(" 3. Edit Order ");
			int choice=Integer.parseInt(bufferedReader.readLine());
			switch (choice) {
				case 1: 
					System.out.println("Enter the type of coffee you wanna enter: ");
					String coffee=bufferedReader.readLine();
					System.out.println("Enter the size of coffee you wanna enter: ");
					String size=bufferedReader.readLine();
					System.out.println("Enter the addOns: ");
					List<String> addOn=new ArrayList<>();
					while(true) {
						String temp=bufferedReader.readLine();
						addOn.add(temp);
						System.out.println("Wanna continue(y/n)");
						String c=bufferedReader.readLine();
						if(c.equals("n"))
							break;
					}
					cs5Implementation.addOrders(addOn,coffee,size);
					break;
				case 2:
					cs5Implementation.show();
					break;
				default:
					System.out.println("Please enter a valid choice");
			}
		}
		
	}
}
