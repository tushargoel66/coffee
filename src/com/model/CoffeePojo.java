package com.model;

import java.util.List;

public class CoffeePojo {
	String coffee,size;
	List<String> addOn;
	float billPrice;
	
	public float getbillPrice() {
		return billPrice;
	}

	public void setbillPrice(float billPrice) {
		this.billPrice = billPrice;
	}

	public String getCoffee() {
		return coffee;
	}
	
	public void setCoffee(String coffee) {
		this.coffee = coffee;
	}
	
	public String getSize() {
		return size;
	}
	
	public void setSize(String size) {
		this.size = size;
	}
	
	public List<String> getAddOn() {
		return addOn;
	}
	
	public void setAddOn(List<String> addOn) {
		this.addOn = addOn;
	}
	
	public CoffeePojo(String coffee, String size, List<String> addOn) {
		this.coffee = coffee;
		this.size = size;
		this.addOn = addOn;
	}
	
	
}
