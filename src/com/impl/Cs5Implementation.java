package com.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.model.CoffeePojo;

import com.repo.Cs5Repo;

public class Cs5Implementation implements Cs5Repo {
	
	private Map<Integer,CoffeePojo> orders;
	private int id;
	
	
	public Map<Integer, CoffeePojo> getOrders() {
		return orders;
	}

	public void setOrders(HashMap<Integer, CoffeePojo> orders) {
		this.orders = orders;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public Cs5Implementation() {
		this.orders = new HashMap<Integer, CoffeePojo>();
		this.id = 0;
	}

	

	@Override
	public void show() {
		
		for(int id:this.orders.keySet()) {
			System.out.println("Order ID= "+id);
			System.out.println("Coffee type= "+this.orders.get(id).getCoffee());
			System.out.println("Coffee size= "+this.orders.get(id).getSize());
			System.out.println("All addOns= "+this.orders.get(id).getAddOn());
		}
		
		
	}

	@Override
	public void addOrders(List<String> addon, String coffee, String size) {
		
		CoffeePojo co=new CoffeePojo(coffee, size, addon);
		this.orders.put(++this.id, co);
		
	}

	
}
